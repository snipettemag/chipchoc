*Looking for a shareable component template? Go here --> [sveltejs/component-template](https://github.com/sveltejs/component-template)*

---

# chipchoc

Create and send cookie-cutter emails

## Get started

Install the dependencies...

```bash
cd chipchoc
yarn
```

...set up the `.env` config file...

```
CHIPCHOC_EMAIL_HOST=mail.example.com
CHIPCHOC_EMAIL_PORT=587
CHIPCHOC_EMAIL_SECURE=false
CHIPCHOC_EMAIL_USER=user@example.com
CHIPCHOC_EMAIL_PASSWORD=badexample
CHIPCHOC_BASE_URL=/
```

...then start [Rollup](https://rollupjs.org):

```bash
nyarn dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running. Edit a component file in `src`, save it, and reload the page to see your changes.

By default, the server will only respond to requests from localhost. To allow connections from other computers, edit the `sirv` commands in package.json to include the option `--host 0.0.0.0`.

## Building and running in production mode

To create an optimised version of the app:

```bash
yarn build
```

You can run the newly built app with `yarn start`. This uses [sirv](https://github.com/lukeed/sirv), which is included in your package.json's `dependencies` so that the app will work when you deploy to platforms like [Heroku](https://heroku.com).

## Credits

* Favicon created by [Freepik](https://www.freepik.com/) from [Flaticon.com](https://www.flaticon.com)
* Cookie pile icon created by [ultimatearm](https://www.freepik.com/authors/ultimatearm) from  [Flaticon.com](https://www.flaticon.com)
* Microwave icon created by [Freepik](https://www.freepik.com/) from [Flaticon.com](https://www.flaticon.com)
* The main ChipChoc icon is a combination of the aforementioned microwave and cookie-pile icons

This is made from a project template for [Svelte](https://svelte.dev) apps.
You can find the original template at https://github.com/sveltejs/template.
